#include <iostream>

#include "Restaurant.hpp"

int main(int, char**)
{
	Restaurant resto = Restaurant::set(10,10,2);
	resto.run(20);
	return 0;
}
