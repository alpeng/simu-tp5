#ifndef RESTAURANT_HPP
#define RESTAURANT_HPP

#include <iostream>
#include <vector>

#include "actors/Client.hpp"
#include "actors/Serveur.hpp"

class Case;

/******************************************************************************\
|* singleton CLASS : Personne                                                 *|
|*                                                                            *|
|*                                                                            *|
\******************************************************************************/
class Restaurant
{
/* --  ATTRIBUTES  ---------------------------------------------------------- */
	protected:
		std::vector<std::vector<Case*>>	 _mat;      // !< tile matrix / map
		std::vector<Serveur*>            _serveurs; // !< waiters list
		std::vector<Client*>             _clients;  // !< customers list
		Case                             _door;     // !< customers' in/out tile
		static Restaurant *              _instance; // !< singleton instance

/* --  CONSTRUCTORS  -------------------------------------------------------- */
	private:
		/**
		 * private constructor
		 *
		 * /param w width of the map
		 * /param h height of the map
		 * /param s number of waiters
		 */
		Restaurant(unsigned w, unsigned h, unsigned s);

	public:
/* --  GETTERS  ------------------------------------------------------------- */
		/**
		 * /brief gets the singleton instance
		 */
		static Restaurant& get_instance() { return *(Restaurant::_instance); }

		/**
		 * /brief gets the width of the map
		 */
		unsigned width() const { return _mat.size(); }

		/**
		 * /brief gets the height of the map
		 */
		unsigned height() const { return _mat[0].size(); }

		/**
		 * /brief gets the number of waiters
		 */
		unsigned nb_serveurs() const { return _serveurs.size(); }

		/**
		 * /brief gets the current number of clients
		 */
		unsigned nb_clients() const { return _clients.size(); }

		/**
		 * /gets the reference to the tile (i,j)
		 *
		 * /param TODO
		 * /param TODO
		 */
		Case& get_case(unsigned i, unsigned j) { return *(_mat[i][j]); }

		/**
		 * /brief gets the in/out waypoint of the customers
		 */
		Case& getDoor() { return _door; }

/* --  SETTERS  ------------------------------------------------------------- */
		/**
		 * /brief resets the restaurants with new parameters
		 *
		 * /param w width of the new map
		 * /param h height of the new map
		 * /param s new number of waiters
		 */
		static Restaurant& set(unsigned w, unsigned h, unsigned s);

/* --  ACTIONS  ------------------------------------------------------------- */
		/**
		 * /brief runs the simulations
		 *
		 * /param number of turns to run
		 */
		void run (unsigned turns);

/* --  DISPLAY  ------------------------------------------------------------- */
		/**
		 * /brief prints the current state of the map
		 *
		 * /param os stream to write on
		 */
		void display(std::ostream &os) const;
};

#endif
