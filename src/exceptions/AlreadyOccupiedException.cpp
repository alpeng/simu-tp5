#include <iostream>
#include "AlreadyOccupiedException.hpp"

/* --  CONSTRUCTORS  -------------------------------------------------------- */
/*
 * AlreadyOccupiedException::AlreadyOccupiedException()
 * defined in header
 */

/* --  DESTRUCTOR  ---------------------------------------------------------- */
/*
 * AlreadyOccupiedException::~AlreadyOccupiedException()
 * defined in header
 */

/* --  DISPLAY  ------------------------------------------------------------- */
const char * AlreadyOccupiedException::what() const noexcept
{
	return "Cette personne ne peut pas être mis à cette place car la case est déjà prise";
}
