#ifndef ALREADYOCCUPIEDEXCEPTION_HPP
#define ALREADYOCCUPIEDEXCEPTION_HPP

#include <exception>
#include <iostream>

/******************************************************************************\
|* CLASS : AlreadyOccupiedException                                           *|
|*                                                                            *|
|* derived from: std::exception                                               *|
|*                                                                            *|
|* exception class returned when a tile (Case) is already occupied by sb      *|
\******************************************************************************/
class AlreadyOccupiedException : public std::exception
{
	public:
/* --  CONSTRUCTORS  -------------------------------------------------------- */
		/**
		 * /brief default constructor
		 */
		AlreadyOccupiedException() {}

/* --  DESTRUCTOR  ---------------------------------------------------------- */
		/**
		 * /brief destructor
		 */
		~AlreadyOccupiedException() {}

/* --  DISPLAY  ------------------------------------------------------------- */
		/**
		 * /brief details what the exception is
		 *
		 * /return a string containing the details
		 */
		virtual const char * what() const noexcept override;
};

#endif
