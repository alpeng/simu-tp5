#include <iostream>
#include <unistd.h>

#include "actors/Serveur.hpp"
#include "map/Cuisine.hpp"
#include "map/Guichet.hpp"
#include "map/Objectif.hpp"
#include "map/Table.hpp"
#include "Restaurant.hpp"

#if __unix__
	#define SLEEP(A)   sleep(A)
	#define CLEAR()    system("clear");
#else
	#include <windows.h> //Config Windows
	#define SLEEP(A)   Sleep(A)
	#define CLEAR()
#endif


/* --  ATTRIBUTES  ---------------------------------------------------------- */
Restaurant * Restaurant::_instance = nullptr;

/* --  CONSTRUCTORS  -------------------------------------------------------- */
Restaurant::Restaurant(unsigned width, unsigned height, unsigned serveurs)
{
	bool simple = true;
	_mat.resize(width);
	unsigned reste = serveurs;
	for (unsigned i = 0; i < width; i++)
	{

		_mat[i].resize(height);
		for(unsigned j = 0; j < height; j++)
		{
			simple = true;
			std::cout << "Coucou " << i*width + j << " i: " << i << " j: " << j;
			if(i == 0 && j == 0)
			{
				std::cout << "  Cuisine" << std::endl;
				_mat[i][j] = new Cuisine(i,j,5);
				simple = false;
			}
			if(i == 1  && (j == width/2 || j == width/2 +1))
			{
				std::cout << "  Guichet" << std::endl;
				_mat[i][j] = new Guichet(i,j);
				simple = false;
			}
			if( (i == height-1 && j == 0) || (i == height-1 && j == width-1))
			{
				std::cout << "  Table" << std::endl;
				_mat[i][j] = new Table(i,j);
				simple = false;
			}
			if(simple)
			{
				std::cout << "  SimpleCase" << std::endl;
				_mat[i][j] = new Case(i,j);
				if(reste > 0)
				{
					std::cout << "  Avec serveur" << std::endl;
					_serveurs.push_back(new Serveur(_mat[i][j], Objectif::guichet));
					_mat[i][j]->personne = _serveurs.back();
					reste--;
				}
			}
		}
	}
	_door = *(_mat[width-1][height/2]);
	_serveurs.resize(serveurs);

}

Restaurant& Restaurant::set(unsigned w, unsigned h, unsigned s)
{

	if(_instance == nullptr)
	{
		_instance = new Restaurant(w,h,s);
	}
	return (*_instance);
}

void Restaurant::display(std::ostream &os) const
{
	CLEAR();
	unsigned width = this->width();
	unsigned height = this->height();
	for(unsigned i = 0; i < width; i++)
	{
		for(unsigned j = 0; j < height; j++)
		{
			os << _mat[i][j]->to_string();
		}
		os << std::endl;
	}

}

void Restaurant::run(unsigned turns)
{

	for(unsigned i = 0; i < turns; i++)
	{
		display(std::cout);
		std::cout << i << std::endl;
		for(unsigned j = 0; j < _serveurs.size(); j++)
		{
			//~ std::cout << "serveur " << j << " i avant" << _serveurs[j]->get_case()->x() <<" j avant "<<_serveurs[j]->get_case()->y()<<std::endl;
			_serveurs[j]->seDeplacer();
			//~ std::cout << "serveur " << j << " i après" << _serveurs[j]->get_case()->x() <<" j après "<<_serveurs[j]->get_case()->y()<<std::endl;
			SLEEP(1);// sans majuscule pour Linux
		}
	}
}
