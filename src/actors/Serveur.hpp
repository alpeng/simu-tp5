#ifndef SERVEUR_HPP
#define SERVEUR_HPP

#include "../colors.h"
#include "Personne.hpp"

/******************************************************************************\
|* CLASS : Serveur                                                            *|
|*                                                                            *|
|* derived from: Personne                                                     *|
|*                                                                            *|
|* waiter class                                                               *|
\******************************************************************************/
class Serveur : public Personne
{
	public:
/* --  CONSTRUCTORS  -------------------------------------------------------- */
		/**
		 * /brief default constructor
		 */
		Serveur() {}

		/**
		 * /brief constructor
		 *
		 * /param place new position
		 * /param o target position
		 */
		Serveur(Case* place, Objectif o): Personne(place,o) {};

/* --  GETTERS  ------------------------------------------------------------- */
		/**
		 * /brief gets the current position of the waiter
		 */
		virtual Case* get_case() const override { return _position; }

/* --  SETTERS  ------------------------------------------------------------- */
		/**
		 * /brief sets a new target position
		 *
		 * /param type of the target position
		 */
		virtual void set_objectif(Objectif o) override;

/* --  ACTIONS  ------------------------------------------------------------- */
		void preparer();
		void servir();

/* --  DISPLAY  ------------------------------------------------------------- */
		/**
		 * /brief returns an 'S'
		 */
		virtual std::string to_string() const override { return FONT_BLACK "S"; }
};

#endif
