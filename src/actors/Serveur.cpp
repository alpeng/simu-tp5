#include <math.h>

#include "../mt19937ar.hpp"
#include "../Restaurant.hpp"
#include "../map/Cuisine.hpp"
#include "../map/Guichet.hpp"
#include "../map/Objectif.hpp"
#include "Serveur.hpp"

void Serveur::set_objectif(Objectif o)
{
	if(o == Objectif::cuisine || o == Objectif::guichet )
	{
		_objectif = o;
	}
}

void Serveur::preparer()
{
    if(_position->get_type() == 0 )
    {
        Cuisine * c = (Cuisine *) _position;
        if(c->get_temps_preparation() >c->get_temps_ecoule())
        {
            c->preparer();
        }
        else
        {
            servir();
        }
    }
}

void Serveur::servir()
{
    if(_position->get_type() == 1)
    {
        Case & g =fromObjectifToCase(_objectif) ;

        ((Guichet &)g).SupprimerClient(((Guichet &)g).getQueue().front());
    }
}
