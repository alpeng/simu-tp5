#include <algorithm>
#include <math.h>
#include <typeinfo>

#include "../Restaurant.hpp"
#include "../exceptions/AlreadyOccupiedException.hpp"
#include "../map/Objectif.hpp"
#include "Personne.hpp"

/* --  CONSTRUCTORS  -------------------------------------------------------- */
/*
 * Personne::Personne()
 * defined in header
 */

Personne::Personne(Personne const& personne)
{
	_position = personne.get_case();
	_objectif = personne.getObjectif();
}

Personne::Personne(Case * place, Objectif o)
{
	if(place->is_occupied())
	{
		throw AlreadyOccupiedException();
	}
	else
	{
		_position = place;
		_objectif = o;
	}
}

/* --  ACTIONS  ------------------------------------------------------------- */
void Personne::seDeplacer()
{

	Case finale(fromObjectifToCase(_objectif));
	int posx = _position->x();
	int posy = _position->y();
	std::cout << "Debut " << finale.x() << " " <<finale.y()<< std::endl;
        std::cout << finale.get_type()<<std::endl;
        std::cout << _position->get_type()<<std::endl;
	Case North;
	Case South;
	Case West;
	Case East;
        class PositionPlace
        {
            public:
                double dist;
                Case c;
                PositionPlace(Case ca,double d):c(ca),dist(d){};
        };
       struct {
            bool operator()(PositionPlace a, PositionPlace b) const
            {
                bool retour= false;
                if(!a.c.is_occupied() && !b.c.is_occupied())
                {
                    retour = a.dist < b.dist;
                }
                else
                {
                    if(!a.c.is_occupied())
                    {
                        retour = true;
                    }
                }


                return retour;
            }
        } comparaison;
        std::vector<PositionPlace> indecision;

	if(posx!=0)
	{
		North = Restaurant::get_instance().get_case(posx - 1, posy);
	}
	else
	{
		North = (*get_case());
	}

	if(posx != Restaurant::get_instance().height() - 1)
	{
		South = Restaurant::get_instance().get_case(posx + 1, posy);
	}
	else
	{
		South = (*get_case());
	}
	if(posy != 0)
	{
		West = Restaurant::get_instance().get_case(posx, posy - 1);
	}
	else
	{
		West = (*get_case());
	}
	if(posy != Restaurant::get_instance().width() -1)
	{
		East = Restaurant::get_instance().get_case(posx, posy + 1);
	}
	else
	{
		East = (*get_case());
	}



	std::cout << "Casepoles" << std::endl;
	Case Next = *_position;
	double distanceNorth;
	double distanceSouth;
	double distanceWest;
	double distanceEast;
	double min;
	bool occupied = true;
	int x;
	int y;
	x = finale.x();
	y = finale.y();
	min = sqrt((double)((posx - x) * (posx - x) + (posy - y) * (posy - y)));;
	if( (*_position)!= finale )
	{
		distanceNorth = sqrt((double)((North.x() - x) * (North.x() - x) + (North.y() - y) * (North.y() - y)));
		distanceSouth = sqrt((double)((South.x() - x) * (South.x() + 1 - x) + (South.y() - y) * (South.y() - y)));
		distanceWest = sqrt((double)((West.x() - x) * (West.x() - x) + (West.y() - 1 - y) * (West.y() - 1 - y)));
		distanceEast = sqrt((double)((East.x() - x) * (East.x() - x) + (East.y() + 1 - y) * (East.y() + 1 - y)));



		/*std::cout << "Actu: "<< posx << " " << posy<< std::endl;
		std::cout << "North: "<< North.x() << " " << North.y()<<"distance: "<< distanceNorth<< std::endl;
		std::cout << "South: "<< South.x() << " " << South.y()<<"distance: "<<distanceSouth << std::endl;
		std::cout << "West: "<< West.x() << " " << West.y()<<"distance: "<< distanceWest<<std::endl;
		std::cout << "East: "<< East.x() << " " << East.y()<<"distance: "<< distanceEast<<std::endl;*/

		occupied = occupied && North.is_occupied();
		if(!(North.is_occupied()) && (min > distanceNorth || occupied))
		{
			min = distanceNorth;
			Next = North;
		}
		if(!(South.is_occupied()) && (min > distanceSouth || occupied))
		{
			min = distanceSouth;
			Next = South;
		}
		occupied = occupied && South.is_occupied();
		if(!(West.is_occupied()) && (min > distanceWest || occupied))
		{
			min = distanceWest;
			Next = West;
		}
		occupied = occupied && West.is_occupied();
		if(!(East.is_occupied()) && (min > distanceEast || occupied))
		{
			min = distanceEast;
			Next = East;
		}
                if(Next.x()==posx && Next.y()==posy &&  *_position != finale )//indécis ou bloqué
                {

                    std::cout << "bloque"<<std::endl;
                    indecision.push_back(PositionPlace(North,distanceNorth ));
                    indecision.push_back(PositionPlace(South,distanceSouth ));
                    indecision.push_back(PositionPlace(East,distanceEast ));
                    indecision.push_back(PositionPlace(West,distanceWest ));
                    std::sort(indecision.begin(),indecision.end(),comparaison);
                    Next = indecision.at(0).c;

                }
	}

	std::cout << "Next: "<< Next.x() << " " << Next.y()<< std::endl;
	_position->personne = nullptr;
	_position = &(Restaurant::get_instance().get_case(Next.x(), Next.y()));
	_position->personne = this;


}
