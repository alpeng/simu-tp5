#include <math.h>

#include "../Restaurant.hpp"
#include "../map/Objectif.hpp"
#include "Client.hpp"

/* --  CONSTRUCTORS  -------------------------------------------------------- */
/*
 * Client::Client()
 * defined in header
 */

/*
 * Client::Client(Case * case, Objectof obj)
 * defined in header
 */

/* --  GETTERS  ------------------------------------------------------------- */
/*
 * Case * Client::get_case() const
 * defined in header
 */

 /* --  SETTERS  ------------------------------------------------------------- */
void Client::set_objectif(Objectif o)
{
	if (o == Objectif::table || o == Objectif::guichet)
	{
		_objectif = o;
	}
}

/* --  DISPLAY  ------------------------------------------------------------- */
/*
 * Client::to_string()
 * defined in header
 */
