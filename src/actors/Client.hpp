#ifndef CLIENT_HPP
#define CLIENT_HPP

#include "../colors.h"
#include "Personne.hpp"

/******************************************************************************\
|* CLASS : Client                                                             *|
|*                                                                            *|
|* derived from: Personne                                                     *|
|*                                                                            *|
|* customer class                                                             *|
\******************************************************************************/
class Client : public Personne
{
	public:
/* --  CONSTRUCTORS  -------------------------------------------------------- */
		/**
		 * /brief default constructor
		 */
		Client() {}

		/**
		 * /brief constructor
		 *
		 * /param place current position
		 * /param obj target action
		 */
		Client(Case * place, Objectif obj): Personne(place, obj) {}

/* --  GETTERS  ------------------------------------------------------------- */
		// TODO: refactor: snake_case
		/**
		 * /brief get current tile/position
		 *
		 * /return current tile
		 */
		virtual Case* get_case() const override { return _position; }

/* --  SETTERS  ------------------------------------------------------------- */
		// TODO: refactor: snake_case
		/**
		 * /brief sets a new target if it is a table or a counter
		 *
		 * /param obj new target to set
		 */
		virtual void set_objectif(Objectif obj) override;

/* --  DISPLAY  ------------------------------------------------------------- */
		/**
		 * /brief returns a 'C'
		 */
		virtual std::string to_string() const override { return " " FONT_RED "C"; }
};

#endif
