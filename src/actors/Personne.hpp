#ifndef PERSONNE_HPP
#define PERSONNE_HPP

#include <iostream>

#include "../map/Case.hpp"

enum class Objectif;

/******************************************************************************\
|* abstract CLASS : Personne                                                  *|
|*                                                                            *|
|*                                                                            *|
\******************************************************************************/
class Personne
{
/* --  ATTRIBUTES  ---------------------------------------------------------- */
	protected:
		Case *   _position; // !< current position
		Objectif _objectif; // !< target position

	public:
/* --  CONSTRUCTORS  -------------------------------------------------------- */
		/**
		 * /brief default constructor
		 */
		Personne() {}

		/**
		 * /brief copy constructor
		 *
		 * /param personne person to copy
		 */
		Personne(Personne const& personne);

		/**
		 * /brief constructor
		 *
		 * /param place current position
		 * /param o target position
		 */
		Personne(Case * place, Objectif o);

/* --  DESTRUCTOR  ---------------------------------------------------------- */
		/**
		 * /brief destructor
		 */
		virtual ~Personne() {}

/* --  GETTERS  ------------------------------------------------------------- */
		virtual Case * get_case() const { return _position; }
		Objectif getObjectif() const { return _objectif; }

/* --  SETTERS  ------------------------------------------------------------- */
		virtual void set_objectif(Objectif o) = 0;

/* --  ACTIONS  ------------------------------------------------------------- */
		virtual void seDeplacer();

/* --  DISPLAY  ------------------------------------------------------------- */
		virtual std::string to_string() const = 0;
};

#endif
