#ifndef TABLE_HPP
#define TABLE_HPP

#include <vector>
#include "Case.hpp"

/******************************************************************************\
|* CLASS : Cuisine                                                            *|
|*                                                                            *|
|* derived from: Case                                                         *|
|*                                                                            *|
|* table tile class                                                           *|
\******************************************************************************/
class Table : public Case
{
/* --  ATTRIBUTES  ---------------------------------------------------------- */
	private:
		int _temps_repas;  // !< time to eat
		int _temps_ecoule; // !< time passed eating

		static std::vector<Table*> __liste_table; // !< instance vector

	public:
/* --  CONSTRUCTORS  -------------------------------------------------------- */
		/**
		 * /brief default constructor
		 */
		Table();
		Table(unsigned i, unsigned j);
		bool manger();
		static Case& tableLibre();
		virtual std::string to_string() const override;

};

#endif
