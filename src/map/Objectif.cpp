#include "../Restaurant.hpp"
#include "Cuisine.hpp"
#include "Guichet.hpp"
#include "Objectif.hpp"
#include "Table.hpp"

Case & fromObjectifToCase(Objectif o)
{
	switch(o)
	{
		case Objectif::file:
			return Guichet::bestGuichet();
		case Objectif::table:
			return Table::tableLibre();
		case Objectif::sortir:
			return Restaurant::get_instance().getDoor();
		case Objectif::guichet:
			return Guichet::guichetLibre();
		case Objectif::cuisine:
			return Cuisine::cuisineLibre();

	}
	return Restaurant::get_instance().getDoor();
}
