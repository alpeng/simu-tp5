#include <algorithm>
#include <limits.h>
#include <math.h>

#include "../Restaurant.hpp"
#include "../actors/Personne.hpp"
#include "Guichet.hpp"
#include "Objectif.hpp"

/* --  ATTRIBUTES  ---------------------------------------------------------- */
std::vector<Guichet*> Guichet::__listeGuichet;

Guichet::Guichet()
{
	_type = 1;
	__listeGuichet.push_back(this);
}

Guichet::Guichet(unsigned x, unsigned y) : Case(x, y)
{
	_type = 1;
	__listeGuichet.push_back(this);
}

Guichet::Guichet(const Guichet& g):Case(g.x(), g.y())
{
	_type = 1;
	_clientsqueue = g.getQueue();
	__listeGuichet.push_back(this);
}

/* --  DESTRUCTOR  ---------------------------------------------------------- */
/*
 * Guichet::~Guichet()
 * defined in header
 */

/* --  GETTERS  ------------------------------------------------------------- */
/*
 * Guichet::getQueue()
 * defined in header
 */

/*
 * Guichet::findendfile()
 * defined in header
 */

Case& Guichet::guichetLibre()
{
	unsigned libre = __listeGuichet.size() +1;
	for(unsigned i = 0; i < __listeGuichet.size(); i++)
	{
		if(!(__listeGuichet[i]->is_occupied()))
		{
			libre = i;
		}
	}

	// TODO: comme Cuisine
	return __listeGuichet[libre]->get_case();
}

Case& Guichet::bestGuichet()
{
	unsigned min_nbclients = INT_MAX;
	int min = 0;
	for(unsigned i = 0; i < __listeGuichet.size(); i++)
	{
		if(min_nbclients > __listeGuichet[i]->_clientsqueue.size())
		{
			min_nbclients = __listeGuichet[i]->_clientsqueue.size();
			min = i;
		}
	}
	return __listeGuichet[min]->findendfile();
}

/* --  CUSTOMER MANAGEMENT  ------------------------------------------------- */
void Guichet::AjouterClient(Client * c)
{
	_clientsqueue.push_back(c);
}

void Guichet::ServirClient(Client *)
{
	// Objectif o;
	//c->set_objectif(o);
}

void Guichet::SupprimerClient(Client * c)
{
	_clientsqueue.erase(find(_clientsqueue.begin(),_clientsqueue.end(),c));
}

/* --  DISPLAY  ------------------------------------------------------------- */
std::string Guichet::to_string() const
{
	std::string personnestring = " ";
	if(personne!=nullptr)
	{
		personnestring = personne->to_string();
	}
	return BG_RED " " + personnestring + " " COLOR_RESET " ";
}
