#ifndef GUICHET_HPP
#define GUICHET_HPP

#include <iostream>
#include <vector>

#include "../Restaurant.hpp"
#include "Case.hpp"

class Client; // TODO: FWD

/******************************************************************************\
|* CLASS : Guichet                                                            *|
|*                                                                            *|
|* derived from: Case                                                         *|
|*                                                                            *|
|* counter tile class                                                         *|
\******************************************************************************/
class Guichet : public Case
{
	private:
/* --  ATTRIBUTES  ---------------------------------------------------------- */
		std::vector<Client*> _clientsqueue; // !< waiting queue

		static std::vector<Guichet*> __listeGuichet; // !< list of all the counters

	public:
/* --  CONSTRUCTORS  -------------------------------------------------------- */
		/**
		 * /brief default constructor
		 */
		Guichet();

		/**
		 * /brief constructor
		 *
		 * /param x x coordinate of the counter
		 * /param y y coordinate of the counter
		 */
		Guichet(unsigned x, unsigned y);

		/**
		 * /brief copy constructor
		 *
		 * /param g counter to copy
		 */
		Guichet(const Guichet& g);

/* --  DESTRUCTOR  ---------------------------------------------------------- */
		/**
		 * /brief destructor
		 */
		~Guichet() {}

/* --  GETTERS  ------------------------------------------------------------- */
		/**
		 * /brief gets the waiting queue
		 *
		 * /return waiting queue
		 */
		std::vector<Client*> getQueue() const { return _clientsqueue; }

		/**
		 * /brief gets the tile of the end of the queue
		 *
		 * /return a tile
		 */
		Case& findendfile() { return Restaurant::get_instance().get_case(_x+_clientsqueue.size(), _y); }

		/**
		 * /brief gets the first available coutner from the list
		 *
		 * /return an available counter
		 */
		static Case& guichetLibre();

		/**
		 * /brief gets the counter with the least amount of customers
		 *
		 * /return a counter
		 */
		static Case& bestGuichet();

/* --  CUSTOMER MANAGEMENT  ------------------------------------------------- */
		void AjouterClient(Client *);
		void ServirClient(Client *);
		void SupprimerClient(Client *);

/* --  DISPLAY  ------------------------------------------------------------- */
		/**
		 * /brief copy operator
		 *
		 * /param c tile to copy
		 * /return a copy of the current tile
		 */
		virtual std::string to_string() const override;
};

#endif
