#include "../colors.h"
#include "../actors/Personne.hpp"
#include "Cuisine.hpp"

/* --  ATTRIBUTES  ---------------------------------------------------------- */
std::vector<Cuisine*> Cuisine::__listeCuisine;

/* --  CONSTRUCTORS  -------------------------------------------------------- */
/*
 * Cuisine::Cuisine()
 * defined in header
 */

/*
 * Cuisine::Cuisine(unsigned i, unsigned j)
 * defined in header
 */

Cuisine::Cuisine(int tempsPrepa)
{
	_type = 0;
	_temps_ecoule = 0;
	_temps_preparation = tempsPrepa;
	__listeCuisine.push_back(this);
}

Cuisine::Cuisine(unsigned i, unsigned j, int tempsPrepa):Case(i, j)
{
	_type = 0;
	_temps_ecoule = 0;
	_temps_preparation = tempsPrepa;
	__listeCuisine.push_back(this);
}

/* --  GETTERS  ------------------------------------------------------------- */
// TODO: redo
Case& Cuisine::cuisineLibre()
{
	int libre = __listeCuisine.size()+1;
	for(unsigned i = 0; i < __listeCuisine.size(); i++)
	{
		if(!(__listeCuisine[i]->is_occupied()))
		{
			libre = i;
		}
	}
	return __listeCuisine[libre]->get_case();
}

/* --  ACTIONS  ------------------------------------------------------------- */
bool Cuisine::preparer()
{
	bool pret = false;
	++_temps_ecoule;

	if(_temps_ecoule == _temps_preparation)
	{
		_temps_ecoule = 0;
		pret = true;
	}

	return pret;
}

/* --  DISPLAY  ------------------------------------------------------------- */
std::string Cuisine::to_string() const
{
	return BG_YELLOW " " + (
		(personne != nullptr) ? personne->to_string() : " "
	) + " " COLOR_RESET " ";
}
