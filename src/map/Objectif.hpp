#ifndef OBJECTIF_HPP
#define OBJECTIF_HPP

#include "Case.hpp"

class Restaurant;

/******************************************************************************\
|* ENUM CLASS : Objectif                                                      *|
|*                                                                            *|
|*                                                                            *|
\******************************************************************************/
enum class Objectif {
	file,
	table,
	sortir,
	guichet,
	cuisine
};

/**
 * /brief gets a tile from a given target
 *
 * /param o target tile to get
 */
Case & fromObjectifToCase(Objectif o);

#endif
