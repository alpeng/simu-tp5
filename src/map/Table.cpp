#include "../colors.h"
#include "../actors/Personne.hpp"
#include "Table.hpp"

std::vector<Table*> Table::__liste_table;

Table::Table()
{
	 _type = 2;
   __liste_table.push_back(this);
}

Table::Table(unsigned i, unsigned j) : Case(i,j)
{
	 _type = 2;
   __liste_table.push_back(this);
}

Case& Table::tableLibre()
{
	int libre = __liste_table.size()+1;
	for(unsigned i = 0; i < __liste_table.size(); ++i)
	{
		if(!(__liste_table[i]->is_occupied()))
		{
			libre = i;
		}
	}
	return __liste_table[libre]->get_case();
}

std::string Table::to_string() const
{
	std::string personnestring = " ";
	if(personne!=nullptr)
	{
		personnestring = personne->to_string();
	}
	return BG_GREEN " " + personnestring + " " COLOR_RESET " ";
}
