#ifndef CUISINE_HPP
#define CUISINE_HPP

#include <vector>
#include "Case.hpp"

/******************************************************************************\
|* CLASS : Cuisine                                                            *|
|*                                                                            *|
|* derived from: Case                                                         *|
|*                                                                            *|
|* kitchen tile class                                                         *|
\******************************************************************************/
class Cuisine : public Case
{
/* --  ATTRIBUTES  ---------------------------------------------------------- */
	private:
		int _temps_preparation; // !< cooking delay
		int _temps_ecoule;      // !< time spent cooking latest meal

		static std::vector<Cuisine*> __listeCuisine; // !< list of all the kitchens

	public:
/* --  CONSTRUCTORS  -------------------------------------------------------- */
		/**
		 * /brief default constructor
		 */
		Cuisine() { __listeCuisine.push_back(this); }

		/**
		 * /brief constructor
		 *
		 * /param x x coordinate of the tile
		 * /param y y coordinate of the tile
		 */
		Cuisine(unsigned x, unsigned y):Case(x, y) { __listeCuisine.push_back(this); }

		// TODO: to comment
		Cuisine(int tempsPrepa);
		Cuisine(unsigned i, unsigned j, int tempsPrepa);

/* --  GETTERS  ------------------------------------------------------------- */
		/**
		 * /brief gets the first available kitchen from the list
		 *
		 * /returns a kitchen
		 */
		static Case& cuisineLibre();

		// TODO: to comment
		int get_temps_preparation() { return _temps_preparation; }
		int get_temps_ecoule() { return _temps_ecoule; }

/* --  ACTIONS  ------------------------------------------------------------- */
		// TODO: comments
		bool preparer();

/* --  DISPLAY  ------------------------------------------------------------- */
		// TODO: in Case --> generic private version
		/**
		 * /brief like Case::to_string but with a different color
		 *
		 * /return if noone is on the tile, a white space, otherwise, person->to_string()
		 */
		virtual std::string to_string() const override;

};

#endif
