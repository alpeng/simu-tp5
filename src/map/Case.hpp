#ifndef CASE_HPP
#define CASE_HPP

#include <iostream>

// TODO: include "Personne.hpp" ??
class Personne;

/******************************************************************************\
|* CLASS : Case                                                               *|
|*                                                                            *|
|* tile class, defines the tiles of the restaurants, on which can people move *|
\******************************************************************************/
class Case
{
/* --  ATTRIBUTES  ---------------------------------------------------------- */
	protected:
		unsigned _x;    // !< x coordinate of the tile
		unsigned _y;    // !< y coordinate of the tile
		int      _type; // TODO: TBD

	public:
		Personne * personne; // !< person positioned on the tile

	public:
/* --  CONSTRUCTORS  -------------------------------------------------------- */
		/**
		 * /brief default constructor
		 */
		Case() {}

		/**
		 * /brief constructor
		 *
		 * /param x x coordinate
		 * /param y y coordinate
		 */
		Case(unsigned i, unsigned j);

		/**
		 * /brief copy constructor
		 */
		Case(const Case&);

/* --  DESTRUCTOR  ---------------------------------------------------------- */
		/**
		 * /brief destructor
		 */
		~Case() {}

/* --  GETTERS  ------------------------------------------------------------- */
		/**
		 * /brief x coordinate getter
		 *
		 * /return x coordinate of the tile
		 */
		unsigned x() const { return _x; }

		/**
		 * /brief y coordinate getter
		 *
		 * /return y coordinate of the tile
		 */
		unsigned y() const { return _y; }

		// TODO: refactor snake_case
		/**
		 * /brief checks if someone is on the tile
		 *
		 * /return true if someone is on the tile
		 */
		bool is_occupied() const { return (personne != nullptr); }

		/**
		 * /brief tile getter (for children classes)
		 *
		 * /return the tile associated with the object
		 */
		Case& get_case() { return *this; }

		/**
		 * /brief gets the type of the tile
		 */
		int get_type() const { return _type; }

/* --  SETTERS  ------------------------------------------------------------- */
		// TODO: supprimer
		/**
		 * /brief sets the type of the tile
		 */
		void type(int t) { _type = t; }

/* --  DISPLAY  ------------------------------------------------------------- */
		/**
		 * /brief tile display (in a console)
		 *
		 * /return if noone is on the tile, a white space, otherwise, person->to_string()
		 */
		virtual std::string to_string() const;

/* --  OPERATORS  ----------------------------------------------------------- */
		/**
		 * /brief copy operator
		 *
		 * /param c tile to copy
		 * /return a copy of the current tile
		 */
		Case& operator=(const Case& dep);

		/**
		 * /brief inequality operator
		 *
		 * /param dep other tile to compare
		 * /return true if the tiles are different
		 */
		bool operator!=(const Case dep);
};

#endif
