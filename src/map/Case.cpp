#include <typeinfo>

#include "../colors.h"
#include "../actors/Personne.hpp"
#include "Case.hpp"

/* --  CONSTRUCTORS  -------------------------------------------------------- */
/*
 * Case::Case()
 * defined in header
 */

Case::Case(unsigned x, unsigned y)
{
	_x = x;
	_y = y;
	_type = -1;
	personne = nullptr;
}

Case::Case(const Case& place)
{
	_x = place.x();
	_y = place.y();
	_type = -1;
	personne = place.personne;
}

/* --  DESTRUCTOR  ---------------------------------------------------------- */
/*
 * Case::~Case()
 * defined in header
 */

/* --  GETTERS  ------------------------------------------------------------- */
/*
 * Case::x()
 * defined in header
 */

/*
 * Case::y()
 * defined in header
 */

/*
 * Case::is_occupied()
 * defined in header
 */

/*
 * Case::get_case()
 * defined in header
 */


/* --  DISPLAY  ------------------------------------------------------------- */
std::string Case::to_string() const
{
	return BG_LIGHT_GREY " " + (
		(personne != nullptr) ? personne->to_string() : " "
	) + " " COLOR_RESET " ";
}

/* --  OPERATORS  ----------------------------------------------------------- */
Case& Case::operator=(const Case& dep)
{
	if(&dep!=this)
	{
		_x = dep.x();
		_y = dep.y();
		_type = dep._type;
		personne = dep.personne;
	}

	return *this;
}

bool Case::operator!=(const Case dep)
{
	return (
		dep.x() != x() ||
		dep.y() != y() ||
		dep.get_type() != get_type()
	);
}
