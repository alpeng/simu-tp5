#ifndef __COLORS_H__
#define __COLORS_H__

#define COLOR_RESET   "\033[0m"

#define FONT_BLACK    "\033[30m"
#define FONT_RED      "\033[31m"

#define BG_RED        "\033[41m"
#define BG_GREEN      "\033[42m"
#define BG_YELLOW     "\033[43m"
#define BG_LIGHT_GREY "\033[47m"

#endif
